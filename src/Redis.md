# Redis

## common instruction

* set key value
e.g. set ray 1234

* keys key 
e.g. keys *

* flushall

* get key
e.g. get ray

* config set dir DirName
e.g. config set dir /root/.ssh

* config get dir

* config set dbfilename FileName
e.g. config set dbfilename authorized_keys

* slaveof IP PORT
e.g. slaveof 10.10.10.160 6379

* info

* save


## common vulnerability

* misconfiguration